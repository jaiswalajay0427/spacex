package org.com.spacex;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

import org.junit.Before;
import org.junit.Test;

//import org.testng.annotations.Test;

public class Tests_SpaceX {

	@Before
	public void hooks() {
		baseURI = "https://api.spacexdata.com/v4";
	}

	// Status Validation
	@Test
	public void Test_Status() {

		given().get("/launches/latest").then().statusCode(200);
	}

	// Schema Validation
	@Test
	public void testJsonSchema() {
		// baseURI = "https://api.spacexdata.com/v4";
		given().get("/launches/latest").then().assertThat().statusCode(200)
				.body(matchesJsonSchemaInClasspath("Schema.json"));
	}

	// Some functionally critical value Validation, for example lets consider
	// flight number and name to be critical here
	@Test
	public void TEst_Value() {
		// baseURI = "https://api.spacexdata.com/v4";

		given().get("/launches/latest").then().statusCode(200).body("flight_number", equalTo(116))
				.body("name", equalTo("Starlink-18 (v1.0)")).log().all();
	}
	/*
	 * @Test public void Test_presenceofValue() { baseURI =
	 * "https://api.spacexdata.com/v4";
	 * 
	 * given() .get("/launches/latest") .then() .statusCode(200) .body("name",
	 * hasItems("Starlink-18 (v1.0)")); }
	 */

}